#include <iostream>
#include <future>

int main() {
  auto fut =
      std::async(std::launch::async, []() {
        std::cout << "Hello from async" <<std::endl;
      });
  fut.wait();
  return 0;
}
